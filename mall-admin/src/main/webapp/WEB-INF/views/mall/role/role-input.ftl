<#--
/****************************************************
 * Description: t_mall_role的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/role/save" id=tabId>
   <input type="hidden" name="id" value="${role.id}"/>
   
   <@formgroup title='name'>
	<input type="text" name="name" value="${role.name}" >
   </@formgroup>
   <@formgroup title='description'>
	<input type="text" name="description" value="${role.description}" >
   </@formgroup>
</@input>